import qualified Data.Maybe
import qualified Data.List
import qualified System.Random
import qualified Control.Monad
import qualified Control.Concurrent.STM
import qualified Graphics.Gloss.Interface.Pure.Game as G
import System.IO 
import System.Environment 
import qualified ParserExt
import qualified Parser
import Game
import Player
import Grid
import Tiles
import Worm
import Tools
import Graphic 

loadFile :: [Cell] -> IO(Game)
loadFile shared = do  
    contents <- readFile "mySave.txt" 
    let [(posStr,suitePos)] = Parser.apply ParserExt.line contents
        [(supStr,suiteSup)] = Parser.apply ParserExt.line suitePos
        [(revStr,suiteRev)] = Parser.apply ParserExt.someLines suiteSup
        [(colStr,suiteCol)] = Parser.apply ParserExt.someLines suiteRev
        [(wormsStr,suiteWorms)] = Parser.apply ParserExt.someLines suiteCol
        [(sStr,suiteS)] = Parser.apply ParserExt.line suiteCol
        [(mStr,suiteM)] = Parser.apply ParserExt.line suiteS
        [(gStr,suiteG)] = Parser.apply ParserExt.line suiteM
        [(tStr,suiteT)] = Parser.apply ParserExt.line suiteG
        [(wStr,suiteW)] = Parser.apply ParserExt.line suiteT
        [(pStr,suiteP)] = Parser.apply ParserExt.line suiteW
        [(lStr,suiteL)] = Parser.apply ParserExt.line suiteP
        [(llStr,suiteLL)] = Parser.apply ParserExt.line suiteL
        [(wlStr,suiteWL)] = Parser.apply ParserExt.line suiteLL
        [(wpStr,suiteWP)] = Parser.apply ParserExt.line suiteWL
        pos = ParserExt.evalPos posStr
        supply = ParserExt.eval supStr
        rev = ParserExt.evalAllPos revStr
        col = ParserExt.evalAllPos colStr
        s = ParserExt.eval sStr
        m = ParserExt.eval mStr
        g = ParserExt.eval gStr
        t = ParserExt.eval tStr
        w = ParserExt.eval wStr
        p = ParserExt.eval pStr
        l = ParserExt.eval lStr
        ll = ParserExt.eval llStr
        wl = ParserExt.eval wlStr
        wp = ParserExt.eval wpStr
        worms = ParserExt.evalAllWorms wormsStr wlStr 
       
    return ( loadGame s m g t w p l ll wl wp supply pos col rev worms  shared )

mPlay :: IO(Game) -> IO()
mPlay game = do
            g <- game
            let grille = grid g
                (c,l) = ((x.player) g,(y.player) g )
                water = checkClosest grille W [] [(c,l)] 0
                portal = checkClosest grille P [] [(c,l)] 0
                tres = checkClosest grille T [] [(c,l)] 0  
                p = player g   
                wormsOut = (map body (worms g))
            putStrLn "\27[2J"
            putStrLn "\27[H"
            putStrLn (show p)
            putStr "The closest water treasor and portal are respectively in "
            print [water,tres,portal]
            displayMap grille 20 p wormsOut
            putStrLn "Please select and action between w a s d"
            cmd <- getChar
            let game = move (getKey cmd) g
            if (isGameOver g) then
               return ()
            else do
               mPlay game

getKey :: Char -> G.Key
getKey cmd 
          | cmd == 'a' = G.SpecialKey G.KeyUp
          | cmd == 'w' = G.SpecialKey G.KeyDown
          | cmd == 'd' = G.SpecialKey G.KeyRight
          | cmd == 's' = G.SpecialKey G.KeyLeft


main = do  
   args <- getArgs  
   if(length args /=  10  && length args /= 0) then do
     putStrLn "Game Over!! Not Enough parameter!"
     return ()
   else do
    shared <- Control.Monad.replicateM 1000000 (Control.Concurrent.STM.newTMVarIO ())
    if (length args == 10) then do
     let s = read (args !! 0) :: Int
         m = read (args !! 1) :: Int
         g = read (args !! 2) :: Int
         t = read (args !! 3) :: Int
         w = read (args !! 4) :: Int
         p = read (args !! 5) :: Int
         l = read (args !! 6) :: Int
         ll= read (args !! 7) :: Int
         wormP = read (args !! 8) :: Int
         wl = read (args !! 9) :: Int
     if (w+p+l>100) || (w+p+ll>100) then do    
       putStrLn "CONSTRAINT(s) FAILED!!! w+p+l<=100 and w+p+ll <=100"
       return ()
     else do
       let   game = buildGame s m g t w p l ll (700,700) wormP 4 shared
       game' <- initWorms game
--       mPlay game
       dispGame  game'
    else do
     game <- loadFile shared
     game' <- initWorms game
     dispGame  game'
