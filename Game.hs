module Game (Game(player,res,width,grid,maxWater,wormPercent,worms,maxWormSize,cells,isGameOver), World, save, initWorms,buildGame ,move,getInfo,loadGame) where 

import qualified Data.List
import qualified Data.Maybe
import qualified System.Random
import qualified Control.Monad
import qualified Graphics.Gloss.Interface.Pure.Game as G
import qualified Control.Concurrent
import qualified Control.Concurrent.STM
import Player
import Grid
import Tiles
import Worm
import Tools

data Game = Game{ s :: Int
                , g :: Int
                , t :: Int
                , w :: Int
                , p :: Int
                , l :: Int
                , ll :: Int
                , player :: Player
                , width :: Int
                , res :: (Int,Int)
                , worms :: [Worm]
                , cells :: [Cell]
                , grid :: Grid
                , maxWater :: Int
                , maxWormSize :: Int
                , wormPercent :: Int
                , isGameOver :: Bool
                , msgs :: [(Control.Concurrent.STM.TMVar Pos, Control.Concurrent.STM.TMVar(Bool,[Pos]))]
                }
type World = IO(Game)

buildGame :: Int -> Int -> Int -> Int -> Int ->  Int -> Int -> Int->  (Int,Int) -> Int -> Int -> [Cell] -> Game
buildGame sv mv gv tv wv pv lv llv  resol  wormP wormS  shared = 
                    Game { s= sv, w = wv, g = gv, l = lv, ll= llv , t= tv, p = pv,
                                       player = gamer, width = 20, res= resol ,worms = ws, maxWater = mv, maxWormSize = wormS, wormPercent = wormP, 
 grid = grille, cells = shared,isGameOver = False, msgs = []}
      where
         gamer = basePlayer sv mv
         rands = generateRands gv 
         ws = replicate 2 (baseWorm wormS)
         grille = generateMap (fromIntegral wv) (fromIntegral pv) (fromIntegral lv) (fromIntegral llv) (fromIntegral tv) rands


loadGame :: Int -> Int -> Int -> Int -> Int -> Int -> Int -> Int -> Int -> Int -> Int -> Pos -> [Pos] -> [Pos] -> [Worm] -> [Cell] -> Game
loadGame sv mv gv tv wv pv lv llv wl wp wLeft playerPos tresCol revPos ws  shared = Game {s= sv, t=tv, w= wv, res= (900,900), g = gv, maxWater = mv, p = pv, l = lv , ll=llv, player=gamer, 
        wormPercent = wp, maxWormSize = wl, cells = shared, worms = ws', grid = grille, width = 20, isGameOver=False,msgs= [] }
     where
          ws' = if length ws == 0 then replicate 2 (baseWorm wl) else ws
          gamer = buildPlayer sv wLeft (fst playerPos) (snd playerPos) tresCol revPos 
          rands = generateRands gv 
          grille = generateMap (fromIntegral wv) (fromIntegral pv) (fromIntegral lv) (fromIntegral llv) (fromIntegral tv) rands
         

initWorms  :: Game -> IO(Game)
initWorms game =  do                   
                      msgs' <- (initMsg.length.worms) game
                      mapM (\(worm,(to,from)) -> do
                                  Control.Concurrent.forkIO $ (moveWorm (cells game) worm (grid game) 20 (((fromIntegral.wormPercent) game )/100.0) to from ) ) $ zip (worms game) msgs'
                      return game{ msgs = msgs'}

--generate here the floats that will be used to set the tiles!!!
generateRands :: Int -> [(Float,Float,Pos)]
generateRands seed = zipWith3 (\ a b c -> (a,b,c)) (1:(System.Random.randoms (System.Random.mkStdGen seed1)) :: [Float]) (1: (System.Random.randoms (System.Random.mkStdGen seed2) :: [Float])) allCoords
               where 
                  allCoords = (generateCoord (0,0))
                  (seed1,gen1) = System.Random.random (System.Random.mkStdGen seed) :: (Int,System.Random.StdGen)
                  (seed2,gen2) = System.Random.random gen1 :: (Int , System.Random.StdGen )

--Set the action 
actOnTile :: Grid -> Int -> Maybe Player -> Maybe Player
actOnTile  tiles _  Nothing = Nothing
actOnTile tiles maxWater (Just p)
              | (tile == W) || (tile == T )||(tile == D) || (tile == L)|| (tile == P) = Just p'
              where
                tile =  extractTile tiles (x p,y p)
                p' = actOnPos p maxWater tile

actOnPos :: Player -> Int -> Tiles -> Player
actOnPos p mWat tile = p{ wsupplies= water, revealedPos = toSee,treasures= tres}
         where
           current = (x p, y p)
           water =  if tile == W then mWat else (wsupplies p)       
           tres = if tile == T then collectTres p else (treasures p)
           seen = revealedPos p
           toSee = seen ` Data.List.union` tileToDisp (x p) (y p) (sight p)

collectTres :: Player -> [Pos]
collectTres p = if current `notElem` collected then current : collected else collected
              where collected = treasures p
                    current = (x p, y p)

isDead :: Grid-> Maybe Player -> [Worm] -> Bool
isDead _ Nothing _= True
isDead tiles (Just p)  worms = onWorm || noWater || onLava
         where pos = (x p,y p)
               onWorm = any (pos `elem` ) $ map body worms
               onLava = extractTile tiles pos == L
               noWater = (wsupplies p) <= 0

isWin :: Grid -> Maybe Player -> Bool
isWin tiles (Just p) = extractTile tiles (x p,y p)== P
isWin tiles Nothing = False

getInfo :: Game -> String
getInfo g = "Water left: "++ (show ((wsupplies.player) g ))++"  Collected tresor: "++ show((length.treasures.player)g )++ "\n next water: "++(show water)++ " next treasure: "++(show tres)++ "\n next portal: "++(show portal)
       where
          grille = grid g
          (c,l) = ((x.player)g,(y.player)g)
          water = checkClosest grille W [] [(c,l)] 0
          portal = checkClosest grille P [] [(c,l)] 0
          tres = checkClosest grille T [] [(c,l)] 0 


toString :: Game -> String
toString game = playerStr ++ wormStr ++ sStr ++ mStr ++ gStr ++ tStr ++ wStr ++ pStr ++ lStr ++ llStr ++ wlStr ++ wpStr
  where
   playerStr = (playerString.player) game 
   wormStr = (wormsToStr.worms)game
   sStr = "\ns ( "++ (show.s) game ++ " )"
   mStr = "\nm ( "++ (show.maxWater) game ++ " )"
   gStr = "\ng ( "++ (show.g) game ++ " )"
   tStr = "\nt ( "++ (show.t) game ++ " )"
   wStr = "\nw ( "++ (show.w) game ++ " )"
   pStr = "\np ( "++ (show.p) game ++ " )"
   lStr = "\nl ( "++ (show.l) game ++ " )"
   llStr = "\nll ( "++ (show.ll) game ++ " )"
   wlStr = "\nx ( "++ (show.maxWormSize) game ++ " )"   
   wpStr = "\ny ( "++ (show.wormPercent) game ++ " )"

tampon :: IO(String) -> Game -> Game
tampon _ g = g

save :: Game -> IO(Game)
save g =  do
              writeFile "mySave.txt" (toString g)
              return g



--will map the buttons to the corresponding action
--dispatch :: [(Char, Player -> Maybe Player)]  
dispatch :: [(G.Key, Player -> Maybe Player)]  
dispatch =  [ (G.SpecialKey G.KeyDown, moveDown)  
            , (G.SpecialKey G.KeyUp, moveUp)  
            , (G.SpecialKey G.KeyLeft, moveLeft)  
            , (G.SpecialKey G.KeyRight, moveRight)  
            ]

initMsg :: Int -> IO[(Control.Concurrent.STM.TMVar Pos, Control.Concurrent.STM.TMVar(Bool,[Pos]))]
initMsg size = do 
                 toS <- Control.Monad.replicateM size Control.Concurrent.STM.newEmptyTMVarIO 
                 fromS <- Control.Monad.replicateM size Control.Concurrent.STM.newEmptyTMVarIO
                 return (zip toS fromS)


chatWithWorms :: Game -> IO(Game)
chatWithWorms game = do                        
                        let
                           msgs' = msgs game 
                           pos = ((x.player) game,(y.player) game)
                        newWs <- mapM (\(w,(to,from )) -> do

                               Control.Concurrent.STM.atomically(Control.Concurrent.STM.putTMVar to pos)                               
                               Control.Concurrent.STM.atomically(                               
                                    Control.Concurrent.STM.orElse ( 
                                     do
--                                        Control.Concurrent.STM.putTMVar to pos
                                        (o,b) <- ( Control.Concurrent.STM.takeTMVar from)
                                        return w{body = b ,out = o}                             
                                       )  
                                      ( return w))

                         ) $ zip (worms game) msgs'
                        Control.Concurrent.threadDelay 300000
                        return game{worms = newWs}


move :: G.Key ->  Game ->  IO(Game)
move cmd game = if not (isMoveOk (player game) cmd ) then do
                 return game 
             else 
               if not( win || dead) then do
                 game' <- chatWithWorms $ game {player = (Data.Maybe.fromJust p')}
                 return game'
               else do
                  return game {isGameOver = True}
      where 
        (Just action) = lookup cmd dispatch              
        p' = actOnTile (grid game)  (maxWater game) $ action (player game)
        win = isWin (grid game) p'
        dead = isDead (grid game) p' (worms game)


