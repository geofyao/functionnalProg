module Graphic (dispGame) where

import qualified Graphics.Gloss
import qualified Graphics.Gloss.Interface.IO.Game as G
import qualified Graphics.Gloss.Data.Color
import qualified Graphics.Gloss.Data.Picture
import qualified Graphics.Gloss.Data.Display
import Tiles
import Grid
import Game
import Player
import Worm

offset :: Int
offset = 100

drawWorld :: Game -> IO(Graphics.Gloss.Data.Picture.Picture)
drawWorld g  
           | not(isGameOver g) = do return (Graphics.Gloss.Data.Picture.pictures [plateau,infos]  )
           | otherwise =  do return (Graphics.Gloss.Data.Picture.pictures [G.color G.red (G.scale 0.15 0.15 (G.text "game over")),infos]  )
                   where 
                       zone = getPlayArea (grid g) (width g) (player g) (map body (worms g))
                       plateau = Graphics.Gloss.Data.Picture.pictures $ setLines 300 $ map (Graphics.Gloss.Data.Picture.pictures) $ map (getRectangle 0) zone
                       infos =  Graphics.Gloss.Data.Picture.translate (-340) (-340) (G.color G.green (G.scale 0.15 0.15 (G.text (getInfo g))))

--getIODraw :: IO(Game) -> IO(Graphics.Gloss.Data.Picture.Picture)
--getIODraw world = fmap drawWorld world


setLines :: Float -> [Graphics.Gloss.Data.Picture.Picture] -> [Graphics.Gloss.Data.Picture.Picture]
setLines y (line:lines) =  (Graphics.Gloss.Data.Picture.translate (-300) y line): setLines (y-30) lines
setLines _ _ = []


getRectangle :: Float -> Grid -> [Graphics.Gloss.Data.Picture.Picture]
getRectangle col (tile:tiles) = rec : ( getRectangle (col+1 * 30) tiles)
                      where 
                        rec =  Graphics.Gloss.Data.Picture.translate col 0  $Graphics.Gloss.Data.Picture.color (getTileColor tile) $ Graphics.Gloss.Data.Picture.rectangleSolid 30 30
getRectangle _ _ = []

getTileColor :: Tiles -> Graphics.Gloss.Data.Color.Color
getTileColor tile = case tile of L -> Graphics.Gloss.Data.Color.red 
                                 W -> Graphics.Gloss.Data.Color.blue 
                                 V -> Graphics.Gloss.Data.Color.green 
                                 H -> Graphics.Gloss.Data.Color.white 
                                 T -> Graphics.Gloss.Data.Color.yellow
                                 D -> Graphics.Gloss.Data.Color.orange
                                 X -> Graphics.Gloss.Data.Color.white 
                                 P -> Graphics.Gloss.Data.Color.black 

--window :: World -> G.Display
--window  world = G.InWindow "Go get it" (res world) (offset, offset)

background :: Graphics.Gloss.Data.Color.Color
background = Graphics.Gloss.Data.Color.black

handleResize :: (Int, Int) -> Game -> Game
handleResize newResolution world =  world { res = newResolution }

handleEvent :: G.Event -> Game -> World
handleEvent event world = case event of
    G.EventResize newResolution -> do return (handleResize newResolution world )
    G.EventKey key state _ _ -> handleKey key state world
    _ -> do return (world)

handleKey :: G.Key -> G.KeyState -> Game -> World
handleKey key state world = case state of
    G.Down -> case key of 
                (G.Char 's') -> save world
                _ -> move key world
    _ -> do return (world)

dispGame :: Game -> IO ()
dispGame world = G.playIO (G.InWindow "Go get it" (1000,1000) (offset, offset)) background 1 world drawWorld handleEvent (\ _ world -> do return (world))

--------------------------------------------

