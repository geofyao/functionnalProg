module ParserExt (Line, line, someLines, eval, evalPos, evalAllPos,evalAllWorms ) where 

import qualified Parser
import Worm

data Line = Natural Int 
               | Supply Line
               | Position Line Line
               | Revealed Line Line
               | Collected Line Line
               | Part Line Line
               | Emerging [Line]
               | Disappearing [Line]
               | S Line
               | M Line
               | T Line
               | G Line
               | P Line
               | W Line
               | L Line
               | WP Line
               | WL Line
               | LL Line   deriving (Show,Eq)



line :: Parser.Parser Line
line = Parser.oneof [natural, position , supply, revealed,collected, bodyPart,eWormBody, dWormBody, s,m,g,t,p,w,l,ll,w,wl,wp]

someLines :: Parser.Parser [Line]
someLines = Parser.oneof [allRev, allCol,allWorm]


natural :: Parser.Parser Line
natural = do Parser.blank
             fmap Natural Parser.fromRead 

collected :: Parser.Parser Line
collected = do Parser.blank
               Parser.keyword "collected ( ["
               left <- line
               Parser.keyword ","
               right <- line
               Parser.keyword "] )"
               return (Collected left right )

revealed :: Parser.Parser Line
revealed = do Parser.blank
              Parser.keyword "revealed ( ["
              left <- line
              Parser.keyword ","
              right <- line
              Parser.keyword "] )"
              return (Revealed left right )

position :: Parser.Parser Line
position = do Parser.blank
              Parser.keyword "position ( ["
              left <- line
              Parser.keyword ","
              right <- line
              Parser.keyword "] )"
              return (Position left right )

bodyPart :: Parser.Parser Line
bodyPart = do Parser.blank
              Parser.keyword "["
              left <- line
              Parser.keyword ","
              right <- line
              Parser.keyword "]"
              return (Part left right)

eWormBody :: Parser.Parser Line
eWormBody = do 
              Parser.blank
              Parser.keyword "emerging ("
              first <-  bodyPart
              allParts <- Parser.many (Parser.keyword "," >> bodyPart)
              Parser.keyword ")"       
              return (Emerging (first :allParts) )     

dWormBody :: Parser.Parser Line
dWormBody = do 
              Parser.blank
              Parser.keyword "disappearing ("
              first <-  bodyPart
              allParts <- Parser.many (Parser.keyword "," >> bodyPart)
              Parser.keyword ")"       
              return (Disappearing (first :allParts) )     

 
allRev :: Parser.Parser [Line]
allRev = Parser.many revealed

allCol :: Parser.Parser [Line]
allCol = Parser.many collected

allWorm :: Parser.Parser [Line]
allWorm = Parser.many (Parser.orelse eWormBody dWormBody)

supply  :: Parser.Parser Line
supply = do Parser.blank
            Parser.keyword "supply ("
            sup <- line
            Parser.keyword ")"
            return $ M sup

s  :: Parser.Parser Line
s =      do Parser.blank
            Parser.keyword "s ("
            sup <- line
            Parser.keyword ")"
            return $ Supply sup

m  :: Parser.Parser Line
m =      do Parser.blank
            Parser.keyword "m ("
            sup <- line
            Parser.keyword ")"
            return $ M sup

g  :: Parser.Parser Line
g =      do Parser.blank
            Parser.keyword "g ("
            sup <- line
            Parser.keyword ")"
            return $ G sup

t  :: Parser.Parser Line
t = do      Parser.blank
            Parser.keyword "t ("
            sup <- line
            Parser.keyword ")"
            return $ T sup

w  :: Parser.Parser Line
w =      do Parser.blank
            Parser.keyword "w ("
            sup <- line
            Parser.keyword ")"
            return $ W sup

p  :: Parser.Parser Line
p =      do Parser.blank
            Parser.keyword "p ("
            sup <- line
            Parser.keyword ")"
            return $ P sup

l  :: Parser.Parser Line
l = do      Parser.blank
            Parser.keyword "l ("
            sup <- line
            Parser.keyword ")"
            return $ L sup

ll  :: Parser.Parser Line
ll = do     Parser.blank
            Parser.keyword "ll ("
            sup <- line
            Parser.keyword ")"
            return $ LL sup

wl  :: Parser.Parser Line
wl = do     Parser.blank
            Parser.keyword "x ("
            sup <- line
            Parser.keyword ")"
            return $ WL sup

wp  :: Parser.Parser Line
wp = do     Parser.blank
            Parser.keyword "y ("
            sup <- line
            Parser.keyword ")"
            return $ WP sup

[(test1,suite1)] = Parser.apply someLines "emerging ([ 10 , 3 ],[ 10 , 2])\nemerging ([ 1 , 1],[ 0 , 1])\n x ( 4 )"
[(test2,suite2)] = Parser.apply line suite1

-------------------------


evalAllWorms :: [Line] -> Line-> [Worm]
evalAllWorms [] _ = []
evalAllWorms (x: xs) l = evalWorm x l : (evalAllWorms xs l)


evalWorm :: Line -> Line -> Worm
evalWorm (Emerging parts) l = buildWorm (eval l) (evalAllPos parts ) True
evalWorm (Disappearing parts) l = buildWorm (eval l) (evalAllPos parts ) False

evalAllPos :: [Line] -> [(Int,Int)]
evalAllPos [] = []
evalAllPos (x: xs) = evalPos x : (evalAllPos xs)


evalPos :: Line -> (Int,Int)
evalPos (Position c l ) = getPos c l
evalPos (Revealed c l ) = getPos c l
evalPos (Collected c l) = getPos c l
evalPos (Part c l) = getPos c l

getPos :: Line -> Line -> (Int,Int)
getPos c l = (x,y)
    where x = eval c
          y = eval l

eval :: Line ->  Int
eval (Supply n) = eval n
eval (S n) = eval n
eval (M n) = eval n
eval (G n) = eval n
eval (T n) = eval n
eval (P n) = eval n
eval (W n) = eval n
eval (L n) = eval n
eval (LL n) = eval n
eval (WL n) = eval n
eval (WP n) = eval n
eval (Natural n) = n

-------Parse File
