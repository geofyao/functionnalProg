module Tools(Pos,Cell,translatePos,posToStr,listPosToStr) where

import qualified Control.Concurrent.STM

type Pos = (Int,Int)
type Cell = Control.Concurrent.STM.TMVar ()

--Translate 2D coords into 1D coord
translatePos :: Pos -> Int
translatePos (x,y) = (x+ (div ((x+y)*(x+y+1)) 2))

posToStr :: Pos -> String
posToStr (a,b) ="[ " ++ (show) a ++ " , "++ (show)b ++ "]"

listPosToStr :: [Pos] -> String
listPosToStr [] = ""
listPosToStr (pos:poss) = (posToStr pos) ++ coma ++ ( listPosToStr poss)
              where
                    coma = if (length poss)>0 then "," else ""  
