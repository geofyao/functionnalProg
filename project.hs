import qualified Data.Array
import qualified Data.List
import qualified System.Random
import System.IO 
import System.Environment
import qualified Data.Maybe 

data Tiles = D | L | W | P | T| X| H deriving (Read,Eq)
type Pos = (Int,Int)
type Grid = [Tiles]
data Player = Player { sight ::Int
                     , wsupplies :: Int
                     , x :: Int
                     , y :: Int
                     , treasures :: [Pos]                     
                     , visitedPos :: [Pos]
                     , revealedPos :: [Pos]
                      }deriving (Show)


instance Show Tiles where
  show T = "$"
  show P = "P"
  show W = "W"
  show D = "D"
  show L = "L"
  show X = "X"
  show H = "."
----------player methods----------
basePlayer :: Int -> Int ->Maybe Player 
basePlayer s m = Just (Player s m 0 0 [] [(0,0)] revealed)
           where 
                revealed = [(c,l)| l<- [0..s] , c<-[0..s] , c+l <= s ]

moveUp :: Maybe Player -> Maybe Player
moveUp (Just p)
    | otherwise = Just Player {sight = (sight p), wsupplies= (wsupplies p - 1), x = (x p), y = (y p +1), treasures =(treasures p), visitedPos = (visitedPos p), revealedPos = (revealedPos p) }

moveDown :: Maybe Player -> Maybe Player
moveDown (Just p) 
   | (y p - 1) >= 0 = Just Player {sight = (sight p), wsupplies= (wsupplies p - 1), x = (x p), y = (y p - 1),  treasures = (treasures p), visitedPos = (visitedPos p), revealedPos = (revealedPos p)}
   | otherwise = Nothing

moveRight :: Maybe Player -> Maybe Player
moveRight (Just p) 
   | otherwise = Just Player {sight = (sight p), wsupplies= (wsupplies p - 1), x = (x p + 1), y = (y p), treasures = (treasures p), visitedPos = (visitedPos p), revealedPos = (revealedPos p)}

moveLeft:: Maybe Player -> Maybe Player
moveLeft (Just p) 
   | (x p - 1) >= 0 = Just Player {sight = (sight p), wsupplies= (wsupplies p - 1), x = (x p - 1), y = (y p), treasures = (treasures p), visitedPos = (visitedPos p), revealedPos = (revealedPos p)}   
   | otherwise = Nothing

isMoveOk :: Maybe Player -> Char -> Bool
isMoveOk (Just p) c
   | c == 'a' = True
   | c == 'w' = (y p - 1) >= 0
   | c == 's' = (x p - 1) >= 0
   | c == 'd' = True
   | otherwise = False

noWatter :: Player -> Bool
noWatter p = (wsupplies p - 1) <= 0

refillWater ::  Int -> Player -> Maybe Player
refillWater a p = Just Player {sight = (sight p), wsupplies= a, x = (x p ), y = (y p),treasures = (treasures p), visitedPos = (visitedPos p), revealedPos = (revealedPos p)}

--Set the action 
actOnTile :: Grid -> Int -> Maybe Player -> Maybe Player
actOnTile  tiles _  Nothing = Nothing
actOnTile tiles maxWater (Just p)
              | tile == W = (refillWater maxWater p')
              | tile == T = Just p'
              | (tile == D) || (tile == L)|| (tile == P) = Just p'
              where
                tile =  extractTile tiles (x p,y p)
                p' = actOnPos p

actOnPos :: Player -> Player
actOnPos p = Player {sight = (sight p), wsupplies = (wsupplies p ), x = (x p ), y = (y p), treasures = (collectTres p),visitedPos = newVisited, revealedPos = toSee }
         where
           current = (x p, y p)        
           seen = revealedPos p
           visited = visitedPos p
           newVisited = if current `elem` visited then visited else  current:(visitedPos p)
           toSee = seen ` Data.List.union` tileToDisp (x p) (y p) (sight p)

collectTres :: Player -> [Pos]
collectTres p = if current `notElem` collected then current : collected else collected
              where collected = treasures p
                    current = (x p, y p)

isDead :: Grid-> Maybe Player -> Bool
isDead tiles (Just p) = (wsupplies p) <= 0 || extractTile tiles (x p,y p)== L
isDead tiles Nothing = True

isWin :: Grid -> Maybe Player -> Bool
isWin tiles (Just p) = extractTile tiles (x p,y p)== P
isWin tiles Nothing = False

-----------------MAP methods----------

generateCoord :: (Int,Int) -> [(Int,Int)]
generateCoord (x,y) = (x,y): generateCoord next
             where 
                next = if y-1<0 then (0,x+1) else (x+1,y-1)

generateMap :: Float -> Float -> Float -> Float -> Float -> [(Float,Float,Pos)] -> Grid
generateMap w p l ll t rands= D:(map (\x -> setTiles (w/100) (p/100) (l/100) (ll/100) (t/100) x) rands)
--generateMap w p l ll t rands= foldr (\x acc -> setTiles acc (w/100) (p/100) (l/100) (ll/100) (t/100) x : acc )[] rands  

--generateMap :: Grid -> Float -> Float -> Float -> Float -> Float -> [(Float,Float,Pos)] -> Grid
--generateMap [] w p l ll t (x:rands)= first ++ generateMap first w p l ll t rands
--                                    where
--                                       first = [(setTiles [] (w/100) (p/100) (l/100) (ll/100) (t/100) x )]

--generateMap pre w p l ll t (x:rands)=  (generateMap pre w p l ll t rands )++ [tile]
--                                    where                                       
--                                       tile = setTiles  pre (w/100) (p/100) (l/100) (ll/100) (t/100) x 
--                                       acc = pre ++ [tile]

--generate here the floats that will be used to set the tiles!!!
generateRands :: Int -> [(Float,Float,Pos)]
generateRands seed = zipWith3 (\ a b c -> (a,b,c)) (1:(System.Random.randoms (System.Random.mkStdGen seed1)) :: [Float]) (1: (System.Random.randoms (System.Random.mkStdGen seed2) :: [Float])) allCoords
               where 
                  allCoords = (generateCoord (0,0))
                  (seed1,gen1) = System.Random.random (System.Random.mkStdGen seed) :: (Int,System.Random.StdGen)
                  (seed2,gen2) = System.Random.random gen1 :: (Int , System.Random.StdGen )

--Set the tile according to the probability
--setTiles :: Grid -> Float -> Float -> Float -> Float -> Float ->(Float,Float,Pos) -> Tiles
--setTiles preGenTiles water portal lava ll treasure (rand,rand1,pos) 
setTiles :: Float -> Float -> Float -> Float -> Float ->(Float,Float,Pos) -> Tiles
setTiles water portal lava ll treasure (rand,rand1,pos) 
   | rand <= water = W
   | rand <= (water + portal) = P
   | rand <= (water + portal+lava)= L
  -- | (noPreLava preGenTiles pos && rand <= (water + portal+ll) )||(rand <= (water + portal+lava))= L -- does not work because of the accumulator
   | otherwise = if rand1 <= (treasure) then T else D

--get a tile from the list
extractTile :: [Tiles] -> (Int,Int) -> Tiles
extractTile matrix (x , y) = matrix !! translatePos (x,y)


--tells whether or not there is lava for the neighbours of the tile at that position
noPreLava :: Grid -> Pos -> Bool
noPreLava [] _ = False
noPreLava preGenTiles pos =  L `elem` ( map(preGenTiles !!)  convertedNeigh)--(map(preGenTiles !!) ())
                           where
                               convertedNeigh = (filter (< (length preGenTiles)) . map(translatePos) . getNeighb2)  pos


getNeighb2 :: (Int,Int) -> [(Int,Int)]
getNeighb2 (x,y) = [(c,l)| c<-[x-1..x+1], l <- [y-1..y+1],c>=0,l>=0,(c,l)/=(x,y),(c,l) `notElem` [(x-1,y-1),(x-1,y+1),(x+1,y+1),(x+1,y-1)]]

--Translate 2D coords into 1D coord
translatePos :: Pos -> Int
translatePos (x,y) = (x+ (div ((x+y)*(x+y+1)) 2))

--from a position and a line sigh
--tell what are the positions that should be seen
squareToDisp :: (Int,Int) -> Int  -> [(Int,Int)]
squareToDisp (x, y) width = [(c,l)| l<- [minY .. (minY + width)] , c<-[minX .. (minX+ width)]]
     where 
      minX = width * (x `div` width)
      minY = width * (y `div` width)


tileToDisp :: Int -> Int -> Int -> [(Int,Int)]
tileToDisp x y s = [(c,l)| l<- [(Data.List.maximum [0 , y-s])..y+s] , c<-[(Data.List.maximum [0 , x-s])..x+s] , abs (abs x - abs c)+ abs (abs y - abs l) <= s ]

printGrid:: [Grid] -> IO[()]
printGrid grid = sequence $ map (putStrLn . textRep) grid

textRep :: Show a => [a] -> String
textRep row = foldl(\acc y -> acc ++ show(y) ++ " ")"" row

displayMap :: Grid -> Int -> Maybe Player -> IO[()]
displayMap grille width (Just p) = do printGrid all
   where
      grille' = map( extractTile grille) ( squareToDisp (x p,y p) width)
      grille'' = buildSquare (width +1) grille'      
      all =   [ [ printTile p (c,l) width ((grille'' !! l)!!c) |c <- [0..(length (grille''!! l) -1)]] | l <- reverse $[0..(length grille''-1)]]

printTile :: Player -> Pos -> Int -> Tiles -> Tiles
printTile p (c,l) width tile 
                  | (c + minX, l+minY) == (x p , y p) = X
                  | (c + minX, l+minY) `elem` (treasures p ) = D
                  | (c + minX, l+minY) `notElem` (revealedPos p ) = H
                  | otherwise = tile
                  where 
                   minX = width * ((x p)`div` width)
                   minY = width * ((y p)`div` width)
                                   
    
          
--Just help to form a 2D list from a 1D list
-- n correspond to the number of columns
buildSquare :: Int -> [a] -> [[a]]
buildSquare n [] = []
buildSquare n subMatrix = a :(buildSquare n b) 
                       where a = fst (splitAt n subMatrix)
                             b = snd (splitAt n subMatrix)

--Can you find this tile at that position
isTile ::  Grid -> Tiles -> (Int,Int) -> Bool
isTile tiles tile pos = extractTile tiles pos == tile


--Used for the search of tiles to find the closest W P or T
checkClosest :: Grid -> Tiles -> [Pos] -> [Pos] -> Int -> Int
checkClosest tiles  tile visited toVisit dist = if  any(isTile tiles tile) toVisit then
                                           dist
                                       else
                                           checkClosest tiles tile (visited++toVisit) newToVisit (dist+1)
                                       where
                                           children = Data.List.nub $ concatMap (getNeighb tiles) toVisit
                                           newToVisit = children Data.List.\\ (visited++toVisit)

--without the diagonal
getNeighb :: Grid ->Pos -> [Pos]
getNeighb tiles (x,y) = [(c,l)| c<-[x-1..x+1], l <- [y-1..y+1],c>=0,l>=0,(c,l)/=(x,y),(c,l) `notElem` [(x-1,y-1),(x-1,y+1),(x+1,y+1),(x+1,y-1)],extractTile tiles (c,l)/=L]

--------------------GAME---------------------


--will map the buttons to the corresponding action
dispatch :: [(Char, Maybe Player -> Maybe Player)]  
dispatch =  [ ('w', moveDown)  
            , ('a', moveUp)  
            , ('s', moveLeft)  
            , ('d', moveRight)  
            ]  
play :: Maybe Player -> Grid -> Int -> IO()
play p grille maxWater = do
            putStrLn "\27[2J"
            putStrLn "\27[H"
            putStrLn (show p)
            let water = checkClosest grille W [] [(x (Data.Maybe.fromJust p), y (Data.Maybe.fromJust p))] 0
            let portal = checkClosest grille P [] [(x (Data.Maybe.fromJust p), y (Data.Maybe.fromJust p))] 0
            let tres = checkClosest grille T [] [(x (Data.Maybe.fromJust p), y (Data.Maybe.fromJust p))] 0            
            putStr "The closest water treasor and portal are respectively in "
            print [water,tres,portal]
            displayMap grille 20 p
            putStrLn "Please select and action between w a s d"
            cmd <- getChar
            if isMoveOk p cmd then do
              let (Just action) = lookup cmd dispatch              
              let p' = actOnTile grille  maxWater $ action p
              let win = isWin grille p'
              let dead = isDead grille p'
              if (not win && not dead) then
               play p' grille maxWater
              else do
               if dead then
                 putStrLn "Game OVER!!!   You Died!!!!!!"
               else 
                 putStrLn "Greaaaat!!! YOU WIN!! Congrats!!!"
               putStrLn ("final state: " ++ (show p))
               return ()
            else do
               putStrLn "Wrong Move!! Try another one please !!"
               play p grille maxWater

--Init of the game!! you need to set the parameters  as arguments while lauching!!!       
main = do  
   args <- getArgs  
   if(length args< 8) then do
     putStrLn "Game Over!! Not Enough parameter!"
     return ()
   else do
     let s = read (args !! 0) :: Int
     let m = read (args !! 1) :: Int
     let g = read (args !! 2) :: Int
     let t = read (args !! 3) :: Float
     let w = read (args !! 4) :: Float
     let p = read (args !! 5) :: Float
     let l = read (args !! 6) :: Float
     let ll= read (args !! 7) :: Float
     if (w+p+l>100) || (w+p+ll>100) then do    
       putStrLn "CONSTRAINT(s) FAILED!!! w+p+l<=100 and w+p+ll <=100"
       return ()
      else do 
       let rands = generateRands g 
       let grille = generateMap w p l ll t rands
       let player = basePlayer s m
       play player grille m



                  
