module Worm (Worm (body,out), baseWorm, buildWorm, moveWorm,wormsToStr) where

import qualified Data.List
import qualified Control.Concurrent
import qualified Control.Concurrent.STM
import qualified System.Random
--import qualified Shuffle
import Tools
import Grid

data Worm = Worm { maxSize :: Int
                 , body :: [Pos]
                 , out :: Bool
                 }deriving Show

baseWorm :: Int -> Worm
baseWorm s = Worm { maxSize = s, body = [], out = True}

buildWorm :: Int -> [Pos] -> Bool -> Worm
buildWorm s parts isOut = Worm { maxSize = s, body = parts, out = isOut}

goOut :: [Cell]-> Worm -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> IO(Worm)
goOut shared w to = do 
             let
                (c,l) = (head.body) w
                possible = [ (c',l')| (c',l')  <- [ (c+1,l), (c-1,l), (c,l+1)  ], c'-1>=0 ]
             pos <- pick possible       
             let
                bodyParts = pos :(body w)
                newOut = (maxSize w) > (length bodyParts) 
                newW = w{body = bodyParts , out = newOut}
             takePosition pos shared newW to
             return (newW)
            

takePosition :: Pos -> [Cell] -> Worm -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> IO()
takePosition pos shared w to = do
     Control.Concurrent.STM.atomically(              
             Control.Concurrent.STM.takeTMVar (shared !! (translatePos pos )) >> Control.Concurrent.STM.putTMVar to (out w , body w))


freePosition :: Pos -> [Cell] -> Worm -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> IO()
freePosition pos shared w to = Control.Concurrent.STM.atomically $

             Control.Concurrent.STM.putTMVar to (out w , body w) >> Control.Concurrent.STM.putTMVar (shared !! (translatePos pos )) ()

goBack :: [Cell]-> Worm -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> IO(Worm)
goBack shared w to = do
              let
                 bodyParts = (init.body) w
                 free = (last.body) w 
                 newOut = 0 ==(length bodyParts) 
                 newW = w{body = bodyParts , out = newOut}
              freePosition free shared newW to
              return(newW)
              

moveWorm :: [Cell]-> Worm -> Grid -> Int -> Float -> Control.Concurrent.STM.TMVar Pos -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> IO()
moveWorm shared w grille width proba from to = do 
                        print("im in: "++ (show w))
                        playerPos <- Control.Concurrent.STM.atomically( Control.Concurrent.STM.takeTMVar from)
                        proba' <- System.Random.randomIO :: IO(Float)
                        if (length.body) w == 0 then do
                               if(proba' <= proba) then do
                                  w' <- start shared w grille width playerPos to
                                  moveWorm shared w' grille width proba from to
                               else
                                  moveWorm shared w grille width proba from to
                        else do
                                w' <- if (out w) then (goOut shared w to ) else (goBack shared w to)
                                moveWorm shared w' grille width proba from to


start :: [Cell]->  Worm -> Grid -> Int -> Pos -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> IO(Worm)
start shared w grille width playerPos to = do
                                selected <- pick ( possibleWormShow  grille playerPos 10)
                                let                                     
                                    newW = w{body = (selected:[]), out = True} 
                                takePosition selected shared newW to
                                return (newW)
                             
pick :: [Pos] -> IO(Pos)
pick pos' = do
              let sup = maximum [0, length(pos')-1]
              n <- System.Random.randomRIO (0, sup :: Int) 
              return (pos' !! n)

wormsToStr :: [Worm]  -> String
wormsToStr [] = ""
wormsToStr (w:ws) = pref ++ (listPosToStr(body w)) ++ ")"   ++ (wormsToStr ws)
        where
              pref = if (out w) then "\nemerging (" else "\ndisappearing ("


