module Tiles (Tiles(D,L,W,P,T,X,H,V)) where

data Tiles = D | L | W | P | T| X| H | V deriving (Read,Eq)
instance Show Tiles where
  show T = "$"
  show P = "P"
  show W = "W"
  show D = "D"
  show L = "L"
  show X = "X"
  show H = "."
  show V = "-"