module Grid (Grid, generateMap, displayMap,checkClosest,generateCoord,tileToDisp,extractTile,squareToDisp,possibleWormShow,getPlayArea) where

import qualified Data.List
import Player
import Tiles
import Tools

type Grid = [Tiles]

generateCoord :: (Int,Int) -> [(Int,Int)]
generateCoord (x,y) = (x,y): generateCoord next
             where 
                next = if y-1<0 then (0,x+1) else (x+1,y-1)

generateMap :: Float -> Float -> Float -> Float -> Float -> [(Float,Float,Pos)] -> Grid
generateMap w p l ll t rands= D:(map (\x -> setTiles (w/100) (p/100) (l/100) (ll/100) (t/100) x) rands)

--Set the tile according to the probability
setTiles :: Float -> Float -> Float -> Float -> Float ->(Float,Float,Pos) -> Tiles
setTiles water portal lava ll treasure (rand,rand1,pos) 
   | rand <= water = W
   | rand <= (water + portal) = P
   | rand <= (water + portal+lava)= L
  -- | (noPreLava preGenTiles pos && rand <= (water + portal+ll) )||(rand <= (water + portal+lava))= L -- does not work because of the accumulator
   | otherwise = if rand1 <= (treasure) then T else D

--get a tile from the list
extractTile :: [Tiles] -> (Int,Int) -> Tiles
extractTile matrix (x , y) = matrix !! translatePos (x,y)


--tells whether or not there is lava for the neighbours of the tile at that position
noPreLava :: Grid -> Pos -> Bool
noPreLava [] _ = False
noPreLava preGenTiles pos =  L `elem` ( map(preGenTiles !!)  convertedNeigh)--(map(preGenTiles !!) ())
                           where
                               convertedNeigh = (filter (< (length preGenTiles)) . map(translatePos) . getNeighb2)  pos


getNeighb2 :: (Int,Int) -> [(Int,Int)]
getNeighb2 (x,y) = [(c,l)| c<-[x-1..x+1], l <- [y-1..y+1],c>=0,l>=0,(c,l)/=(x,y),(c,l) `notElem` [(x-1,y-1),(x-1,y+1),(x+1,y+1),(x+1,y-1)]]


--from a position and a line sigh
--tell what are the positions that should be seen
squareToDisp :: (Int,Int) -> Int  -> [(Int,Int)]
squareToDisp (x, y) width = [(c,l)| l<- [minY .. (minY + width)] , c<-[minX .. (minX+ width)]]
     where 
      minX = width * (x `div` width)
      minY = width * (y `div` width)


tileToDisp :: Int -> Int -> Int -> [(Int,Int)]
tileToDisp x y s = [(c,l)| l<- [(Data.List.maximum [0 , y-s])..y+s] , c<-[(Data.List.maximum [0 , x-s])..x+s] , abs (abs x - abs c)+ abs (abs y - abs l) <= s ]

printGrid:: [Grid] -> IO[()]
printGrid grid = sequence $ map (putStrLn . textRep) grid

textRep :: Show a => [a] -> String
textRep row = foldl(\acc y -> acc ++ show(y) ++ " ")"" row

displayMap :: Grid -> Int -> Player -> [[Pos]] -> IO[()]
displayMap grille width p ws = do printGrid all
   where
      grille' = map( extractTile grille) ( squareToDisp (x p,y p) width)
      grille'' = buildSquare (width +1) grille'      
      all =   [ [ printTile p (c,l) ws width ((grille'' !! l)!!c) |c <- [0..(length (grille''!! l) -1)]] | l <- reverse $[0..(length grille''-1)]]

getPlayArea :: Grid -> Int -> Player -> [[Pos]] -> [Grid]
getPlayArea grille width p ws = all
          where 
               grille' = map( extractTile grille) ( squareToDisp (x p,y p) width)
               grille'' = buildSquare (width +1) grille'      
               all =   [ [ printTile p (c,l) ws width ((grille'' !! l)!!c) |c <- [0..(length (grille''!! l) -1)]] | l <- reverse $[0..(length grille''-1)]]

printTile :: Player -> Pos -> [[Pos]] -> Int -> Tiles -> Tiles
printTile p (c,l) ws width tile 
                  | any ((c,l) `elem` ) ws = V
                  | (c + minX, l+minY) == (x p , y p) = X
                  | (c + minX, l+minY) `elem` (treasures p ) = D
                  | (c + minX, l+minY) `notElem` (revealedPos p ) = H
                  | otherwise = tile
                  where 
                   minX = width * ((x p)`div` width)
                   minY = width * ((y p)`div` width)
                                   
possibleWormShow :: Grid -> Pos -> Int  -> [Pos]
possibleWormShow grille pos width = [ okPos | okPos <- squareToDisp pos width, (extractTile grille okPos) == D , okPos /= pos]
          
--Just help to form a 2D list from a 1D list
-- n correspond to the number of columns
buildSquare :: Int -> [a] -> [[a]]
buildSquare n [] = []
buildSquare n subMatrix = a :(buildSquare n b) 
                       where a = fst (splitAt n subMatrix)
                             b = snd (splitAt n subMatrix)

--Can you find this tile at that position
isTile ::  Grid -> Tiles -> (Int,Int) -> Bool
isTile tiles tile pos = extractTile tiles pos == tile


--Used for the search of tiles to find the closest W P or T
checkClosest :: Grid -> Tiles -> [Pos] -> [Pos] -> Int -> Int
checkClosest tiles  tile visited toVisit dist = if  any(isTile tiles tile) toVisit then
                                           dist
                                       else
                                           checkClosest tiles tile (visited++toVisit) newToVisit (dist+1)
                                       where
                                           children = Data.List.nub $ concatMap (getNeighb tiles) toVisit
                                           newToVisit = children Data.List.\\ (visited++toVisit)

--without the diagonal
getNeighb :: Grid ->Pos -> [Pos]
getNeighb tiles (x,y) = [(c,l)| c<-[x-1..x+1], l <- [y-1..y+1],c>=0,l>=0,(c,l)/=(x,y),(c,l) `notElem` [(x-1,y-1),(x-1,y+1),(x+1,y+1),(x+1,y-1)],extractTile tiles (c,l)/=L]
