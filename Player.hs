module Player (Player,Pos, basePlayer, buildPlayer, moveUp, moveDown, moveLeft, moveRight, isMoveOk, noWatter,refillWater,x, y,treasures, revealedPos, wsupplies, sight,playerString ) where 

import Tools
import qualified Graphics.Gloss.Interface.Pure.Game as G

data Player = Player { sight ::Int
                     , wsupplies :: Int
                     , x :: Int
                     , y :: Int
                     , treasures :: [Pos]       
                     , revealedPos :: [Pos]
                      }deriving (Show)


 ----------player methods----------

buildPlayer :: Int -> Int -> Int -> Int -> [Pos] -> [Pos] -> Player 
buildPlayer s w c l tres rev= Player { sight = s, wsupplies= w , x = c, y = l, treasures = tres, revealedPos = rev}
 
basePlayer :: Int -> Int -> Player 
basePlayer s m = (Player s m 0 0 [] revealed)
           where 
                revealed = [(c,l)| l<- [0..s] , c<-[0..s] , c+l <= s ]

moveUp :: Player -> Maybe Player
moveUp p = Just p {wsupplies= (wsupplies p - 1), y = (y p +1)}

moveDown :: Player -> Maybe Player
moveDown p 
   | (y p - 1) >= 0 = Just p { wsupplies= (wsupplies p - 1), y = (y p - 1)}
   | otherwise = Nothing

moveRight :: Player -> Maybe Player
moveRight p
   | otherwise = Just p {wsupplies= (wsupplies p - 1), x = (x p + 1)}

moveLeft:: Player -> Maybe Player
moveLeft p 
   | (x p - 1) >= 0 = Just p {wsupplies= (wsupplies p - 1), x = (x p - 1)}   
   | otherwise = Nothing

isMoveOk :: Player -> G.Key -> Bool
isMoveOk p c
   | c == G.SpecialKey G.KeyUp = True
   | c == G.SpecialKey G.KeyDown = (y p - 1) >= 0
   | c == G.SpecialKey G.KeyLeft = (x p - 1) >= 0
   | c == G.SpecialKey G.KeyRight = True
   | otherwise = False



noWatter :: Player -> Bool
noWatter p = (wsupplies p - 1) <= 0

refillWater ::  Int -> Player -> Maybe Player
refillWater a p = Just p {wsupplies= a}

playerString :: Player -> String
playerString p = supplies ++ position ++ revealed ++ collected
      where 
           supplies = "supply ( "++ (show.wsupplies) p ++ " )"
           position = posToString [(x p, y p)] "\nposition " " " --"position ( [ " ++ (show.x) p ++ " , "++ (show.y) p ++ "] ) \n"
           revealed = posToString (revealedPos p)"\nrevealed" " "
           collected = posToString (treasures p)" \ncollected" " "

posToString :: [Pos] -> String-> String -> String
posToString [] pref s = s
posToString (rev:revs) pref s = posToString revs pref (s ++  str)
      where 
          str =  pref ++ " ( [ " ++ (show.fst) rev ++ " , "++ (show.snd) rev ++ "] )"
 






