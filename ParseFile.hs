import qualified ParserExt
import qualified Parser
import System.IO
import Game

test :: IO()
test = do  
    contents <- readFile "mySave.txt" 
    let [(posStr,suitePos)] = Parser.apply ParserExt.line contents
        [(supStr,suiteSup)] = Parser.apply ParserExt.line suitePos
        [(revStr,suiteRev)] = Parser.apply ParserExt.someLines suiteSup
        [(colStr,suiteCol)] = Parser.apply ParserExt.someLines suiteRev
        [(wormsStr,suiteWorms)] = Parser.apply ParserExt.someLines suiteCol
        [(sStr,suiteS)] = Parser.apply ParserExt.line suiteWorms
        [(mStr,suiteM)] = Parser.apply ParserExt.line suiteS
        [(gStr,suiteG)] = Parser.apply ParserExt.line suiteM
        [(tStr,suiteT)] = Parser.apply ParserExt.line suiteG
        [(wStr,suiteW)] = Parser.apply ParserExt.line suiteT
        [(pStr,suiteP)] = Parser.apply ParserExt.line suiteW
        [(lStr,suiteL)] = Parser.apply ParserExt.line suiteP
        [(llStr,suiteLL)] = Parser.apply ParserExt.line suiteL
        [(wlStr,suiteWL)] = Parser.apply ParserExt.line suiteLL
        [(wpStr,suiteWP)] = Parser.apply ParserExt.line suiteWL
        pos = ParserExt.evalPos posStr
        supply = ParserExt.eval supStr
        rev = ParserExt.evalAllPos revStr
        col = ParserExt.evalAllPos colStr
        s = ParserExt.eval sStr
        m = ParserExt.eval mStr
        g = ParserExt.eval gStr
        t = ParserExt.eval tStr
        w = ParserExt.eval wStr
        p = ParserExt.eval pStr
        l = ParserExt.eval lStr
        ll = ParserExt.eval llStr
        wl = ParserExt.eval wlStr
        wp = ParserExt.eval wpStr
        worms = ParserExt.evalAllWorms wormsStr wlStr 
    print (pos) 
    print (supply)
    print (rev)
    print (col)
    print (s) 
    print (m)
    print (g)
    print (t)
    print (w) 
    print (p)
    print (l)
    print (ll)
    print (wl) 
    print (wp)    
    print (worms)
    return ()
