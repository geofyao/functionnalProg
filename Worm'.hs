module Worm (Worm (body,out), baseWorm, buildWorm, moveWorm, start, pickNextPos) where

import qualified Data.List
import qualified Control.Concurrent
import qualified Control.Concurrent.STM

--import qualified Shuffle
import Tools
import Grid

data Worm = Worm { maxSize :: Int
                 , body :: [Pos]
                 , out :: Bool
                 }deriving Show

baseWorm :: Int -> Worm
baseWorm s = Worm { maxSize = s, body = [], out = True}

buildWorm :: Int -> [Pos]  -> Worm
buildWorm s parts = Worm { maxSize = s, body = parts, out = (length parts == 0)}

goOut :: [Cell]-> Worm -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> Control.Concurrent.STM.STM(Worm)
goOut shared w to = do 
             let
                pos = (pickNextPos.body) w
                bodyParts = pos :(body w)
                newOut = (maxSize w) >= (length bodyParts) 
                newW = w{body = bodyParts , out = newOut}
             takePosition pos shared newW to
             return (newW)
            

takePosition :: Pos -> [Cell] -> Worm -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> Control.Concurrent.STM.STM()
takePosition pos shared w to = do Control.Concurrent.STM.takeTMVar (shared !! (translatePos pos )) >> Control.Concurrent.STM.putTMVar to (out w , body w)



freePosition :: Pos -> [Cell] -> Worm -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> Control.Concurrent.STM.STM()
freePosition pos shared w to = do Control.Concurrent.STM.putTMVar to (out w , body w) >> Control.Concurrent.STM.putTMVar (shared !! (translatePos pos )) ()

goBack :: [Cell]-> Worm -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> Control.Concurrent.STM.STM(Worm)
goBack shared w to = do
              let
                 bodyParts = (init.body) w
                 free = (last.body) w 
                 newOut = 0 >= (length bodyParts) 
                 newW = w{body = bodyParts , out = newOut}
              freePosition free shared newW to
              return(newW)
              

moveWorm :: [Cell]-> Worm -> Grid -> Int -> Control.Concurrent.STM.TMVar Pos -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> IO()
moveWorm shared w grille width from to = do 
                        print("im in: "++ (show w))
                        nexts <- if(length.body) w == 0 then 
                            then 
                        w' <- Control.Concurrent.STM.atomically( 
                           do
                             playerPos <- Control.Concurrent.STM.takeTMVar from
                             if (length.body) w == 0 then do
                                start shared w grille width playerPos to
                             else do
                                if (out w) then (goOut shared w to ) else (goBack shared w to))
                        moveWorm shared w' grille width from to



start :: [Cell]->  Worm -> Grid -> Int -> Pos -> Control.Concurrent.STM.TMVar(Bool,[Pos]) -> Control.Concurrent.STM.STM(Worm)
start shared w grille width playerPos to = do
                                let 
                                    availables = possibleWormShow grille playerPos width
                                    selected = availables !! 1
                                    newW = w{body = (selected:[]), out = True} 
                                takePosition selected shared newW to
                                return (newW)
                             
pick [Pos] -> IO(Pos)
pick pos' = do
              let sup = maximum [0, length(pos')]
              n <- System.Random.randomIO (0, sup)
              return (pos' !! n)
 
pickNextPos :: [Pos] -> Pos
pickNextPos allParts = possible !! 1
      where (c,l) = Data.List.head allParts
            possible = {-Shuffle.shuffleM $-}  [ pos'| pos'  <- [ (c+1,l), (c-1,l), (c,l+1) ], c >= 0, l >= 0]

